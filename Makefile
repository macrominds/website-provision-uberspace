website-docs-all-but-docroot:
	ansible-playbook -i website-docs, site.yml --skip-tags=set_docroot

website-docs:
	ansible-playbook -i website-docs, site.yml

website-docs-test:
	ansible-playbook -i website-docs-test, site.yml

reqs:
	ansible-galaxy install -r requirements.yml

force-reqs:
	ansible-galaxy install -r requirements.yml  --force
